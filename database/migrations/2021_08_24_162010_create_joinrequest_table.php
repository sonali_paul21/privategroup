<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJoinrequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joinrequest', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('join_group_id');
            $table->string('approval_amount');
            $table->string('approved_by');
            $table->string('approval_validity');
            $table->enum('approval_status',['pending','wait','active'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joinrequest');
    }
}
