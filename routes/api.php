<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
    
});

/*----------- Frontend Routes ----------- */
Route::post('auth/login', ['uses' => 'AuthController@authenticate', 'as' => 'login']);
Route::post('register',   ['uses' => 'AuthController@register', 'as' => 'register']);
Route::post('forgot_password',   ['uses' => 'AuthController@forgot_password', 'as' => 'forgot_password ']);


Route::group(['middleware' => ['jwt.verify']], function () {
    Route::post('get_user', ['uses' => 'AuthController@getAuthenticateUser', 'as' => 'get_user']);
    Route::post('auth/logout',   ['uses' => 'AuthController@logout', 'as' => 'logout']);
    Route::post('user/send_group_join_request',   ['uses' => 'Frontend\UserApiController@send_join_request', 'as' => 'send_join_request']);
    Route::post('user/update_wp_number',   ['uses' => 'Frontend\UserApiController@update_wp_number', 'as' => 'update_wp_number']);
    Route::post('user/update_educationdetails',   ['uses' => 'Frontend\UserApiController@update_educationdetails', 'as' => 'update_educationdetails']);
    Route::post('get_user_group_info',   ['uses' => 'Frontend\UserApiController@get_user_group_info', 'as' => 'get_user_group_info']);
    Route::post('group/book_appointment',   ['uses' => 'Frontend\UserApiController@book_appointment', 'as' => 'book_appointment']);
    Route::post('send_mail_advisor',   ['uses' => 'Frontend\UserApiController@send_mail_advisor', 'as' => 'send_mail_advisor ']);
    Route::post('create_post',   ['uses' => 'Frontend\UserApiController@create_post', 'as' => 'create_post']);
    Route::post('all_posts',   ['uses' => 'Frontend\UserApiController@all_posts', 'as' => 'all_posts']);
    Route::get('group_info/{slug}', 'Frontend\UserApiController@get_group_info')->name('user.get_group');
    Route::post('post_comment',   ['uses' => 'Frontend\UserApiController@post_comment', 'as' => 'post_comment']);
    Route::post('recent_comments',   ['uses' => 'Frontend\UserApiController@recent_comments', 'as' => 'recent_comments']);


});
/* --------- End Frontend Routes -------- */

/* ---------- Start Admin Routes --------- */

Route::group(['prefix'=>'admin'],function () {
    Route::get('/profile', 'Admin\AdminController@profile')->name('admin.profile');
});
Route::get('/users', 'Admin\AdminController@users')->name('admin.users');
Route::get('/roles', 'Admin\AdminController@roles')->name('admin.roles');
Route::get('/permissions', 'Admin\AdminController@permissions')->name('admin.permissions');
Route::post('/create/user', 'Admin\AdminController@create_user')->name('admin.create_user');
Route::post('/create/role', 'Admin\AdminController@create_role')->name('admin.create_role');
Route::post('/create/permission', 'Admin\AdminController@create_permission')->name('admin.create_permission');
Route::post('/create/group', 'Admin\AdminController@create_group')->name('admin.create_group');
Route::post('/uploads', 'Admin\AdminController@uploads')->name('admin.uploads');
Route::get('/groups', 'Admin\AdminController@all_groups')->name('admin.all_groups');
Route::post('/delete/user', 'Admin\AdminController@delete_user')->name('admin.delete_user');
Route::get('/get/user/{id}', 'Admin\AdminController@get_user')->name('admin.get_user');
Route::post('/update/user/{id}', 'Admin\AdminController@update_user')->name('admin.update_user');
Route::post('/update/user_password/{id}', 'Admin\AdminController@user_password')->name('admin.user_password');
Route::get('/get/group/{id}', 'Admin\AdminController@get_group')->name('admin.get_group');
Route::post('/delete/group', 'Admin\AdminController@delete_group')->name('admin.delete_group');
Route::post('/group/join_requests', 'Admin\AdminController@join_requests')->name('group.join_requests');
Route::post('/group/approve_request', 'Admin\AdminController@approve_request')->name('group.approve_request');
Route::post('/group/decline_join_request', 'Admin\AdminController@decline_join_request')->name('group.decline_join_request');
Route::post('/group/update/{id}', 'Admin\AdminController@update_group')->name('group.update_group');
Route::post('/success_join', 'PaypalController@success_join')->name('success_join');
Route::get('/all_join_requests', 'Admin\AdminController@all_join_requests')->name('all_join_requests');
Route::get('/privacy_policy_add', 'Admin\AdminController@privacy_policy_add')->name('privacy_policy_add');
Route::get('/about_us_add', 'Admin\AdminController@about_us_add')->name('about_us_add');
Route::post('/store_cms', 'Admin\AdminController@store_cms')->name('store_cms');
Route::post('/get_cms', 'Admin\AdminController@get_cms')->name('get_cms');
Route::post('/upload_cms', 'Admin\AdminController@upload_cms')->name('upload_cms');
Route::post('contact_quote',   ['uses' => 'Frontend\FrontendController@contact_quote', 'as' => 'contact_quote']);



/*----------------- End Admin Routes ------------- */
