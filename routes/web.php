<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.home');
// });

    
// Admin Routes  //

Route::get('/administrator/login', 'Auth\LoginController@login_page')->name('administrator.login');
Route::post('/customLogin', 'Auth\LoginController@custom_login')->name('custom_login');
Route::get('/{joinid}/{userid}/groupjoin/payment/{groupid}', 'PaypalController@payment')->name('groupjoin.payment');
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::group(['middleware'=>['auth','admin']],function(){
        // Route::get('/dashboard', 'Admin\AdminController@dashboard')->name('admin.dashboard');
        // Route::any('/user_list', 'Admin\AdminController@user_list')->name('admin.userlist');
        // Route::any('/profile/{id}', 'Admin\AdminController@edit_user')->name('admin.edit_user');
        // Route::any('/add_user', 'Admin\AdminController@add_user')->name('admin.add_user');
        // Route::any('/user_roles', 'Admin\AdminController@user_roles')->name('admin.user_roles');
        // Route::any('/add/permissions', 'Admin\AdminController@add_permission')->name('admin.add_permission');
        // Route::any('/create/group', 'Admin\AdminController@group_form')->name('admin.group_form');
        // Route::any('/groups', 'Admin\AdminController@groups')->name('admin.groups');
        // Route::any('/groups_details/{id}', 'Admin\AdminController@groups_details')->name('admin.groups_details');
        // Route::any('/join_requests', 'Admin\AdminController@join_requests_page')->name('admin.join_requests_page');
        // Route::any('/cms/privacy-policy', 'Admin\AdminController@cms_privacy_policy')->name('admin.cms_privacy_policy');
        // Route::any('/cms/about-us', 'Admin\AdminController@cms_about_us')->name('admin.cms_about_us');
        // Route::any('/cms/add-cms-page', 'Admin\AdminController@cms_page_add')->name('admin.cms_page_add');

        Route::get('/dashboard', 'Admin\AdminController@render_page')->name('admin.dashboard');
        Route::any('/user_list', 'Admin\AdminController@render_page')->name('admin.userlist');
        Route::any('/profile/{id}', 'Admin\AdminController@render_page')->name('admin.edit_user');
        Route::any('/add_user', 'Admin\AdminController@render_page')->name('admin.add_user');
        Route::any('/user_roles', 'Admin\AdminController@render_page')->name('admin.user_roles');
        Route::any('/add/permissions', 'Admin\AdminController@render_page')->name('admin.add_permission');
        Route::any('/create/group', 'Admin\AdminController@render_page')->name('admin.group_form');
        Route::any('/groups', 'Admin\AdminController@render_page')->name('admin.groups');
        Route::any('/groups_details/{id}', 'Admin\AdminController@render_page')->name('admin.groups_details');
        Route::any('/join_requests', 'Admin\AdminController@render_page')->name('admin.join_requests_page');
        Route::any('/cms/privacy-policy', 'Admin\AdminController@render_page')->name('admin.cms_privacy_policy');
        Route::any('/cms/about-us', 'Admin\AdminController@render_page')->name('admin.cms_about_us');
        Route::any('/cms/add-cms-page', 'Admin\AdminController@render_page')->name('admin.add-cms-page');

    });
});

Route::group(['prefix' => 'user','middleware'=>['auth','user']], function () {
    Route::get('/dashboard', 'Admin\AdminController@dashboard')->name('user.dashboard');
});

// Frontend Routes  //

Route::get('/login', 'PagesController@login')->name('frontend.login');
Route::get('/signup', 'PagesController@signup')->name('frontend.signup');


Route::get('/', 'PagesController@home')->name('frontend.home');
Route::get('/profile', 'PagesController@render_frontend')->name('frontend.profile');
Route::get('/group/{slug}', 'PagesController@render_frontend')->name('frontend.group');
Route::get('/group/{group_id}/member/{member_id}', 'PagesController@render_frontend')->name('frontend.group_member');
Route::get('/privacy-policy', 'PagesController@render_frontend')->name('frontend.privacy_policy');
Route::get('/about-us', 'PagesController@render_frontend')->name('frontend.about_us');
Route::get('/contact-us', 'PagesController@render_frontend')->name('frontend.contact-us');
Route::get('/terms-conditions', 'PagesController@render_frontend')->name('frontend.terms-conditions');
//Route::get('/add-cms-page', 'PagesController@cms')->name('frontend.cms');



