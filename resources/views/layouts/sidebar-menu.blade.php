<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <router-link to="/admin/dashboard" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                    Dashboard
                </p>
            </router-link>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link">
                <i class="nav-icon fas fa-users-cog blue"></i>  
                <p>
                    User Management
                    <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
					<router-link to="/admin/user_list" class="nav-link">
                        <i class="fas fa-user"></i>
                        <p>  Users</p>
                    </router-link>
                </li>
                <li class="nav-item">
					<router-link to="/admin/user_roles" class="nav-link">
                        <i class="fas fa-pencil-ruler"></i> 
                        <p> Roles</p>
                    </router-link>
                </li>
                <li class="nav-item">
					<router-link to="/admin/add/permissions" class="nav-link">
                        <i class="fas fa-tools"></i> 
                        <p> Permissions</p>
                    </router-link>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link">
                <i class="nav-icon fas fa-list blue"></i>
                <p>
                    Group Management
                    <i class="fas fa-angle-left right"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
					<router-link to="/admin/groups" class="nav-link">
                        <i class="fas fa-users"></i> 
                        <p>  Groups </p>
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/admin/join_requests" class="nav-link">
                        <i class="fas fa-users"></i> 
                        <p>  Join Requests </p>
                    </router-link>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link">
                <i class="fas fa-cogs blue" ></i>
                <p>  CMS</p>
                <i class="fas fa-angle-left right"></i>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
					<router-link to="/admin/cms/privacy-policy" class="nav-link">
                    <i class="fas fa-key"></i> 
                        <p>  Privacy Policy </p>
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/admin/cms/about-us" class="nav-link">
                    <i class="far fa-address-card"></i>
                        <p>  About Us </p>
                    </router-link>
                </li>
                <li class="nav-item">
					<router-link to="/admin/cms/add-cms-page" class="nav-link">
                    <i class="fas fa-key"></i> 
                        <p> Add New</p>
                    </router-link>
                </li>
            </ul>
        </li>
        
        <li class="nav-item">
            <a href="#" class="nav-link" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>
                    {{ __('Logout') }}
                </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>
