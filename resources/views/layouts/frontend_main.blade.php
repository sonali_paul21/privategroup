<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Front | Private Group</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="theme-color" content="#0e1f33">
        <meta name="description" content="">
        <meta name="robots" content="index">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!--CSS -->
        <link rel="stylesheet" href="{{ asset('public/css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/assets/frontend/css/animate.min.css')}}">
        <link  rel="stylesheet" href="{{asset('public/assets/frontend/css/aos.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/assets/frontend/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/assets/frontend/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/assets/frontend/css/responsive.css')}}">
        <title>Relationship coach Mario Wolfgang Fröhlich | Home</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/assets/frontend/images/favicon.ico')}}">

	</head>
	<body class="hold-transition sidebar-mini layout-fixed">
		<div id="app">
            <router-view></router-view>
		</div>
	</body>

	<script src="{{asset('public/js/app.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/aos.js')}}"></script>
    <script src="{{asset('public/assets/frontend/js/custom.js')}}"></script>
    
	<html>
