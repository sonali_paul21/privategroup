require('./bootstrap');

window.Vue = require('vue');


import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';
import VueToast from 'vue-toast-notification';
import VueCarousel from 'vue-carousel';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
import carousel from 'vue-owl-carousel';
import { VuejsDatatableFactory } from 'vuejs-datatable';
import 'vue-toast-notification/dist/theme-sugar.css';
import routes from './routes';
import { Form, HasError, AlertError } from 'vform';


import Swal from 'sweetalert2';
import Vue2Editor from "vue2-editor";


Vue.use( VuejsDatatableFactory );
Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(VueToast);
Vue.use(VueCarousel);
Vue.use(Vue2Editor);

Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.component('carousel', carousel);

window.Form = Form;


// Vue.API_BASE_URL = Vue.prototype.API_BASE_URL = 'https://dev8.ivantechnology.in/privategroup/api/';
// Vue.SITE_URL = Vue.prototype.SITE_URL = 'https://dev8.ivantechnology.in/privategroup/';
// Vue.ASSET_URL = Vue.prototype.ASSET_URL = 'https://dev8.ivantechnology.in/privategroup/public/assets/';
// Vue.UPLOADS_URL = Vue.prototype.UPLOADS_URL = 'https://dev8.ivantechnology.in/privategroup/public/';

Vue.API_BASE_URL = Vue.prototype.API_BASE_URL = 'http://localhost/private_group/api/';
Vue.SITE_URL = Vue.prototype.SITE_URL = 'http://localhost/private_group/';
Vue.ASSET_URL = Vue.prototype.ASSET_URL = 'http://localhost/private_group/public/assets/';
Vue.UPLOADS_URL = Vue.prototype.UPLOADS_URL = 'http://localhost/private_group/public/'; 
Vue.isLoading = Vue.prototype.isLoading = false;




const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
window.Swal = Swal;
window.Toast = Toast;

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  });

// Vue.component(HasError.name, HasError)
// Vue.component(AlertError.name, AlertError)

Vue.component('edit-button', {
    template: `
            < button class= "btn btn-xs btn-primary" > Edit</button >
            `
});
Vue.mixin({
    methods: {
    backtoprevious: function () {
        this.$router.go(-1);
      },
    },
});
const router = new VueRouter({
    mode: 'history',
    base: 'private_group',
    routes: routes
});

router.beforeEach((to, from, next) => {
    let authusertoken = localStorage.getItem('user-auth-token');
    // console.log(to,1);
    // console.log(authusertoken,2);
    if (to.matched.some(record => record.meta.skipAuth) === true) {
        next()
    } else {
        if (to.matched.some(record => record.meta.requiresAuth) === true) {
            if (authusertoken != null) {
                next()
            } else {
                next({ name: 'front_login' })
            }
        } else if (to.matched.some(record => record.meta.requiresAuth) === false) {
            if (authusertoken == null) {
                next()
            } else {
                next({ path: '/profile' })
            }
        } else {
            next()
        }
    }
})

const app = new Vue({
    el: '#app',
    router: router
});


