export default [
    // Admin Routes Start//
    {
            path: '/admin/dashboard',
            name: 'dashboard',
            component: require('./components/admin/Dashboard').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/administrator/login',
            name: 'administrator_login',
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/user_list',
            name: 'userlist',
            component: require('./components/admin/Userlist.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/profile/:id',
            name: 'edit_user',
            component: require('./components/admin/Edituser.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/add_user',
            name: 'add_user',
            component: require('./components/admin/Adduser.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/user_roles',
            name: 'user_roles',
            component: require('./components/admin/UserRoles').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/add/permissions',
            name: 'permissions',
            component: require('./components/admin/AddPermission.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/create/group',
            name: 'create_group',
            component: require('./components/admin/CreateGroup.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/groups',
            name: 'groups',
            component: require('./components/admin/GroupList.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/groups_details/:id',
            name: 'GroupDetails',
            component: require('./components/admin/GroupDetails.vue').default,
            meta:{ skipAuth: true}
        },
        {
            path: '/admin/join_requests',
            name: 'join_requests',
            component: require('./components/admin/JoinRequests.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/admin/cms/privacy-policy',
            name: 'privacy-policy',
            component: require('./components/admin/PrivacyPolicyCMS.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/admin/cms/about-us',
            name: 'about-us',
            component: require('./components/admin/AboutUsCMS.vue').default,
            meta: { skipAuth: true }
        },

        {
            path: '/admin/cms/add-cms-page',
            name: 'add-cms-page',
            component: require('./components/frontend/AddCmsPage.vue').default,
            meta: { skipAuth: true }
        },
        

        // Admin Routes End//
        // Front end routes Start//
        {
            path: '/login',
            name: 'front_login',
            component: require('./components/frontend/Login.vue').default,
            meta: { requiresAuth: false }
        },
        {
            path: '/signup',
            name: 'front_signup',
            component: require('./components/frontend/Signup').default,
            meta: { requiresAuth: false }
        },
        // {
        //     path: '/',
        //     name: 'home',
        //     component: Home,
        //     meta: { requiresAuth: false }
        // },
        {
            path: '/',
            name: 'landing',
            component: require('./components/frontend/HomePage.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/profile',
            name: 'profile',
            component: require('./components/frontend/Profile.vue').default,
            meta: { requiresAuth: true }
        },
        {
            path: '/group/:slug',
            name: 'group',
            component: require('./components/frontend/Group.vue').default,
            props: true ,
            meta: { requiresAuth: true }
        },
        {
            path: '/group/:group_id/member/:member_id',
            name: 'groupMember',
            component: require('./components/frontend/GroupMember.vue').default,
            meta: { requiresAuth: true }
        },
        {
            path: '/:joinid/:userid/groupjoin/payment/:groupid',
            name: 'payment',
            component:  require('./components/Paypal.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/thankyou',
            name: 'thankyou',
            component: require('./components/Thankyou.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/privacy-policy',
            name: 'privacy-policy',
            component: require('./components/frontend/PrivacyPolicy.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/about-us',
            name: 'about-us',
            component: require('./components/frontend/AboutUs.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/contact-us',
            name: 'contact-us',
            component: require('./components/frontend/Contact-us.vue').default,
            meta: { skipAuth: true }
        },
        {
            path: '/terms-conditions',
            name: 'terms-conditions',
            component: require('./components/frontend/Terms-conditions.vue').default,
            meta: { skipAuth: true }
        }
        
        
];