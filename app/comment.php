<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table = "comment";

    protected $fillable = [
        'user_id', 'post_id','comment'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id')->with("profile_picture","user_details");
    }
    public function post(){
        return $this->hasOne('App\post','post_id');
    }
    public function getCreatedAtAttribute($value)
    {
        return date('d M y - h:i A', strtotime($value));
    }
}
