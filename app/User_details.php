<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_details extends Model
{
    protected $fillable = [
        'user_id', 'whatsapp_number','address','about_user'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
