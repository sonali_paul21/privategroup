<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_education_details extends Model
{
    protected $fillable = [
        'user_id', 'highest_qualification','institute_name','basic_details'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
