<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joinrequests extends Model
{
    protected $fillable = [
        'user_id', 'group_id','approval_amount','approved_by','approval_validity','approval_status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function group()
    {
        return $this->belongsTo('App\Group','group_id')->with("profile_picture");
    }
    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }
}
