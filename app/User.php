<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Permission;
class User extends Authenticatable implements JWTSubject {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'contact_number','dob','gender','password','occupation','deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsTo('App\Role','role_id');
    }
    public function permissions()
    {
        return $this->belongsTo('App\Permission','permission_id');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function profile_picture()
    {
        return $this->hasOne('App\Uploads','respective_id')->where(["module_name"=>"user","type"=>"profile_image"]);
    }
    public function user_permission(){
        return $this->hasMany('App\User_permission','user_id')->with(['permissions','roles']);
    }
    public function user_details()
    {
        return $this->hasOne('App\User_details','user_id');
    }
    public function user_education_details()
    {
        return $this->hasOne('App\User_education_details','user_id');
    }
}
