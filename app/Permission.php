<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    
    protected $table = 'permission';

    protected $fillable = [
        'permission_name', 'permission_slug',
    ];

    public function permissions(){
        return $this->hasMany('App\User','permission_id');
    }
}
