<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $fillable = [
        'user_id', 'group_id','role_id','is_active'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','user_id')->with('profile_picture');
    }
    public function role()
    {
        return $this->belongsTo('App\Role','role_id');
    }
    public function group()
    {
        return $this->belongsTo('App\Group','group_id')->with('profile_picture');
    }
    public function getCreatedAtAttribute($value)
    {
        return date('d M y - h:i A', strtotime($value));
    }
}
