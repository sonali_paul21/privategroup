<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact_quote extends Model
{
    protected $fillable = [
        'name', 'email', 'phone','message'
    ];
}
