<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $fillable = [
        'user_id', 'request_id','transaction_id','payer_email','payer_id','payment_for'
    ];

    
}
