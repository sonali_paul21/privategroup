<?php

    namespace App\Helpers;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use Auth;
    use App\User;
    use App\Role;
    use App\Permission;
    use App\Group;
    use App\Uploads;
    use App\User_permission;
    use App\Joinrequests;
    use Illuminate\Support\Facades\DB;
    use Mail;

    class Helper{
        public static function  mail($view,$data,$to,$name,$from,$from_name,$mail_subject,$attachement = ""){
            Mail::send($view, ['data' => $data], function ($m) use ($to,$name,$from,$from_name,$mail_subject,$attachement) {
                $m->from($from, $from_name);
                $m->to($to, $name)->subject($mail_subject);
            });
            if (Mail::failures()) {
                return false;
            }
            return true;
        }
    
    }


?>