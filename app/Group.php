<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'group_name', 'group_slug', 'group_charge','is_active'
    ];

    public function profile_picture()
    {
        return $this->hasOne('App\Uploads','respective_id')->where(["module_name"=>"groups","type"=>"profile_image"]);
    }
    
    public function members()
    {
        return $this->hasMany('App\Members','group_id')->with('user','role');
    }
   
}
