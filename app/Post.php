<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post";

    protected $fillable = [
        'user_id', 'group_id','text_content','images'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id')->with("profile_picture","user_details");
    }
    public function group(){
        return $this->hasOne('App\Group','id');
    }
    public function comment(){
        return $this->hasMany('App\Comment','post_id')->with("user")->orderBy("created_at", "DESC")->take(2);
    }
    public function getCreatedAtAttribute($value)
    {
        return date('d M y - h:i A', strtotime($value));
    }
}
