<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Role;
use App\Permission;
use App\Group;
use App\Uploads;
use App\User_permission;
use App\Joinrequests;
use App\Members;
use App\User_details;
use App\User_education_details;
use App\Appointments;
use App\Post;
use App\Comment;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Helpers\Helper as Helper;
class UserApiController extends Controller
{
    //

     public function send_join_request(Request $request)
    {
        //return $request->all();
        $validator = Validator::make($request->all(), [
            "user_id"=> "required",
            "group_id" => "required"
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }

        $group = Group::find($request->group_id);
        $data = [
            'user_id'=> $request->user_id,
            'group_id'=> $request->group_id,
            'approval_amount'=> $group['group_charge'],
            'approved_by'=> "",
            'approval_validity'=> date("Y-m-d",strtotime("+2 days")),
            'user_id'=> $request->user_id,
            'approval_status'=> "pending"
        ];

        $find_data = Joinrequests::where(['user_id'=> $request->user_id,'group_id'=> $request->group_id,])->get()->toArray();
        
        if(!empty($find_data) ){
            $is_active = $find_data[0]["approval_validity"];
            if(date("Y-m-d") < $is_active){
                return "Already sent";
            }else{
                $join = Joinrequests::create($data);
            }
        }else{
            $join = Joinrequests::create($data);
        }
        
        return request()->json([
            "message"=> "Request Sent",
            "status"=> 200
        ]);

    }

    public function update_wp_number(Request $request){
        $validator = Validator::make($request->all(), [
            "user_id"=> "required",
            "wp_number" => "required"
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $message = "";
        $user_exist = User_details::where("user_id",$request->user_id)->first();
        if($user_exist){
           
            try{
                User_details::where("user_id",$request->user_id)->update(["whatsapp_number"=>$request->wp_number]);
                $message = "Updated";
                $status = 200;
            }
            catch(\Illuminate\Database\QueryException $ex){
                $message = $ex->getMessage();
                $status = 400;
            }
            
        }else{
            try{
                User_details::create(["user_id"=>$request->user_id,"whatsapp_number"=>$request->wp_number]);
                $message = "Added";
                $status = 200;
            }
            catch(\Illuminate\Database\QueryException $ex){
                $message = $ex->getMessage();
                $status = 400;
            }
        }
        return response()->json([
            "message" => $message,
            "status" => $status,
        ]);

    }

    public function update_educationdetails(Request $request){
        $validator = Validator::make($request->all(), [
            "user_id"=> "required",
            "highest_qualification" => "required",
            "institute_name" => "required"
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $user_exist = User_education_details::where("user_id",$request->user_id)->first();
        $data_set = [
            "highest_qualification"=>$request->highest_qualification,
            "institute_name"=> $request->institute_name
        ];
        if($user_exist){
            try{
                User_education_details::where("user_id",$request->user_id)->update($data_set);
                $message = "Updated";
                $status = 200;
            }
            catch(\Illuminate\Database\QueryException $ex){
                $message = $ex->getMessage();
                $status = 400;
            }
        }else{
            $data_set["user_id"] = $request->user_id;
            try{
                User_education_details::create($data_set);
                $message = "Added";
                $status = 200;
            }
            catch(\Illuminate\Database\QueryException $ex){
                $message = $ex->getMessage();
                $status = 400;
            }
        }
        return response()->json([
            "message" => $message,
            "status" => $status,
        ]);
    }

    public function get_user_group_info(Request $request){
        $id = Auth::id();
        $members = Members::with("group","role")->where("user_id",$id)->get()->toArray();
        $join_requests = Joinrequests::with("group")->where("approval_validity",">", date("Y-m-d"))->where("approval_status","=","pending")->where("user_id",$id)->get()->toArray();
        
        //select * from pg_groups where id not in ( select group_id from pg_members where user_id = 2)

        $groups = Group::with('profile_picture')
        ->whereNotIn('id', function($query) use($id) {
            $query->select('group_id')
                  ->from('members')
                  ->whereRaw('user_id ='.$id);
        })
        ->whereNotIn('id',function($query) use($id){
            $query->select('group_id')
                ->from('joinrequests')
                ->where('approval_validity','>',date("Y-m-d"))
                ->whereRaw('user_id ='.$id);
        })
        ->get();

        //return  $groups;
        return response()->json([
            "my_group"=>$members,
            "join_requests"=>$join_requests,
            "suggested_group" =>$groups
        ]);
    }

    public function book_appointment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "date_time"=> "required",
            "group_id" => "required",
            "member_id" => "required"   
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        //return Auth::id();
        $data = [
            "user_id"=> Auth::id(),
            "group_id"=>$request->group_id,
            "booking_member_id"=>$request->member_id,
            "date_time_of_appointment"=>$request->date_time,
            "booking_amount"=> 10

        ];
        try{
            Appointments::create($data);
            $message = "Booked Successfully";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status" => $status,
        ]);

    }

    public function send_mail_advisor(Request $request){
        $validator = Validator::make($request->all(), [
            'mail_body' => 'required',
            'to_email'=>'required|email'
        ]);
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $user = User::find(Auth::id());
        $mail_data = [
            "name" => $user->name,
            'mail_body'=>$request->mail_body
        ];
        $mail = Helper::mail("email_templates.advisor_email",$mail_data,$request->to_email,$user->name,$user->email,$user->name,"Mail From User");
        if($mail){
            return response()->json([
                "message"=> "Your mail sent to the advisor successfully !!",
                "status"=> 200
            ]);
        }
        
    }

    public function create_post(Request $request){
        $files = [];
        $data = [];
        $files_arr = [];
        $status = 400;
        $message = "";
        $validator = Validator::make($request->all(), [
            'group_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
       
       if($request->hasfile('images')){
           $files = $request->file('images');
            // echo "<pre>";
            // print_r($files);
            $post = [
                "user_id"=> Auth::id(),
                "group_id"=> $request->group_id
            ];
            $data = Post::create($post);
            foreach($files as $file_key => $file_value){
                $destinationPath = 'uploads/group/post/'.$data->id;
                File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
                $file_name = time().rand(1,100).$file_value->getClientOriginalName();
                $file_value->move('public/'.$destinationPath,$file_name);
                array_push($files_arr,$destinationPath.'/'.$file_name);
            }
            try{
                $data = Post::where("id", $data->id)->update(["images"=>$files_arr,"text_content"=>$request->text_content ]);
                $status = 200;
                $message = "Posted Successfully !";
            }catch(\Illuminate\Database\QueryException $ex){
                $status = 400;
                $message =  $ex->getMessage();
            }
       }else{
            $post = [
                "user_id"=> Auth::id(),
                "group_id"=> $request->group_id,
                "text_content"=> $request->text_content,
            ];
            try{
                $data = Post::create($post);
                $status = 200;
                $message = "Posted Successfully !";
            }catch(\Illuminate\Database\QueryException $ex){
                $status = 400;
                $message =  $ex->getMessage();
            }
       }

        return response()->json([
            "message"=> $message,
            "status"=> 200,
            "data" => $data 
        ]);
    }
    public function all_posts(Request $request){
       
        $validator = Validator::make($request->all(), [
            'group_slug' => 'required'
        ]);
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }

        $get_posts = [];
        $status = 400;
        $message = "";
        try{
            $get_user =  Group::where("group_slug",$request->group_slug)->first();
            //dd($get_user);
            $get_posts = Post::with('user','comment')->where("group_id",$get_user->id)->orderBy('id', 'DESC')->get()->toArray(); 
            $images_arr = [];
            $data_arr = [];
            foreach($get_posts as $value){
                if(!empty($value["images"])){
                    $value["image_arr"] =json_decode($value["images"],1);
                }else{
                    $value["image_arr"] = [];
                }
                array_push($data_arr,$value);
            }
            $status = 200;
            $message = "Posted Successfully !";
        }catch(\Illuminate\Database\QueryException $ex){
            $status = 400;
            $message =  $ex->getMessage();
        }
        return response()->json([
            "message"=> $message,
            "status"=> 200,
            "data" => $data_arr 
        ]);
    }

    public function get_group_info(Request $request){
        $slug = $request->route('slug');
        $get_user = [];
        try{
            $get_user = Group::with('profile_picture','members')->where("group_slug",$slug)->first();
            $message = "Get group Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $get_user 
        ]);
    }
    
    public function post_comment(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'post_id' => 'required',
            'comment'=>'required'
        ]);
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $data_arr  = [
            "user_id"=> $request->user_id,
            "post_id"=> $request->post_id,
            "comment"=> $request->comment,
        ];
        $data = [];
        try{
            $data = Comment::create($data_arr);
            $status = 200;
            $message = "Comment Posted Successfully !";
        }catch(\Illuminate\Database\QueryException $ex){
            $status = 400;
            $message =  $ex->getMessage();
        }
        return response()->json([
            "message"=> $message,
            "status"=> 200,
            "data" => $data
        ]);
    }

    public function recent_comments(Request $request){
        $validator = Validator::make($request->all(), [
            'post_id' => 'required'
        ]);
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $data = [];
        try{
            $data = Comment::where("post_id",$request->post_id)->orderBy("created_at","DESC")->take('2')->get();
            $status = 200;
            $message = "Get Comment Successfully !";
        }catch(\Illuminate\Database\QueryException $ex){
            $status = 400;
            $message =  $ex->getMessage();
        }
        return response()->json([
            "message"=> $message,
            "status"=> 200,
            "data" => $data
        ]);
    }
}
