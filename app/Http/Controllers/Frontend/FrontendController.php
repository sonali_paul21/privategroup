<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Contact_quote;
use App\Helpers\Helper as Helper;

class FrontendController extends Controller
{
    public function contact_quote(Request $request)
    {
        $user = [];
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone'=> 'required|numeric|min:10',
            'message'=> 'required'
        ]);
        //Send failed response if request is not valid
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        //Request is valid, create new user
        $user_arr = [
        	'name' => $request->name,
        	'email' => $request->email,
        	'phone' => $request->phone,
        	'message' => $request->message,
            'contact_number'=>$request->contact_number
            
        ];
        $message = "";
        $status = "";
        try{
            $user = Contact_quote::create($user_arr);
            $message = "Quote Request Sent Successfully !!";
            $status = 200;
            $mail_data = [
                "name" =>  $request->name,
                "phone"=> $request->phone,
                'mail_body'=> $request->message
            ];
            $mail = Helper::mail("email_templates.contact-quote",$mail_data,"privategroupAdmin@yopmail.com",$request->name,$request->email,$request->name,"Quote Request");
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message"=> $message,
            "status"=> 200
        ]);
    }
   
    
}
