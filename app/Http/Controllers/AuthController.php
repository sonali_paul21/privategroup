<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\User;
use App\Uploads;
use App\User_permission;
use Auth;
use Mail;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper as Helper;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $user = [];
        $data = $request->only('name', 'email', 'password','contact_number','dob','gender');
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:8',
            'occupation'=> "required",
            'contact_number'=> 'required|numeric|min:10|unique:users,contact_number',
            'dob'=> 'required',
            'gender'=> 'required'
        ]);

        //Send failed response if request is not valid
        if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }

        //Request is valid, create new user
        $user_arr = [
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => bcrypt($request->password),
        	'occupation' => $request->occupation,
            'contact_number'=>$request->contact_number,
            'dob'=> date('Y-m-d',strtotime($request->dob)),
            'gender' => $request->gender
            
        ];

        try{
            $user = User::create($user_arr);
            $message = "User Created Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }

        if($status == 200){
            $data_set = [
                'module_name' => 'user',
                'respective_id' =>  $user->id,
                'type'=>'profile_image',
                'path'=> 'images/noimage.jpg'
            ];
            try{
                User_permission::create(["user_id"=> $user->id,"permission_id"=>4,"role_id"=>2 ]);
                Uploads::create($data_set);
                $message = "User Created Successfully!";
                $status = 200;
            }catch(\Illuminate\Database\QueryException $ex){
                $message = $ex->getMessage();
                $status = 400;
            }
        }
        
        $token = JWTAuth::fromUser($user);

        //User created, return success response
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $user,
            'token' => $token,
            'status' => $status
        ]);
    }
 
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages(),
                'status'=> 400
            ]);
        }

        //Request is validated
        //Crean token

        $user = User::where("email",$request->email)->first();
    
        if(!empty($user) && $user->id == 1 ){
            return response()->json([
                'success' => false,
                'message' => 'Login credentials are invalid.',
                'status'=> 400
            ]);
        }
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                    'status'=> 400
                ]);
            }
        } catch (JWTException $e) {
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                    'status'=> 500
            ]);
        }
        return response()->json([
            'success' => true,
            'token' => $token,
            'status'=> 200,
            'message'=> "Logged in successfully!"
        ]);
    }
 
    public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages(),"status"=>400]);
        }
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User has been logged out',
                'status' => 200

            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out',
                'status' => 400
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAuthenticateUser(Request $request)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json([]);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        $user_details = User::with("profile_picture","user_permission","user_details","user_education_details")->find($user->id);
        return response()->json([
            "message"=> "Get user successfully",
            "data" => $user_details,
            "status"=> 200
        ]);
    }

    public function forgot_password(Request $request){
        $credentials = $request->only('email');
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
        ]);
        $user = User::where("email",$request->email)->first();
        if(empty($user) || $user->id == 1){
            return response()->json([
                "message"=> "User email not belongs to our record! ",
                "data" =>[],
                "status"=> 400
            ]);
        }else{
            $password = str_random(8);
            User::where("email",$request->email)->update(["password"=>Hash::make($password)]);
            $new_password = rand(0,8);
            $mail_data = [
                "name" => $user->name,
                "password"=> $password
            ];
            $mail = Helper::mail("email_templates.forgot-password",$mail_data,$user->email,$user->name,config('mail.from.address'),config('mail.from.name'),"Forgot Password");
            if($mail){
                return response()->json([
                    "message"=> "New password sent to the given email id",
                    "status"=> 200
                ]);
            }
        }
    }
}