<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Transactions;
use App\User;
use App\Role;
use App\Permission;
use App\Group;
use App\Uploads;
use App\User_permission;
use App\Joinrequests;
use App\Members;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Crypt;

/** Paypal Details classes **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use PayPal\Exception\PayPalConnectionException;
use Exception;

class PaypalController extends Controller
{
    private $api_context;

    public function __construct()
    {
        $this->api_context = new ApiContext(new OAuthTokenCredential(config('paypal.client_id') , config('paypal.secret')));
        $this
            ->api_context
            ->setConfig(config('paypal.settings'));
    }

    /** This method sets up the paypal payment.
     *
     */
    public function createPayment(Request $request)
    {
        //dd($request->all());
        //Setup Payer
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        //Setup Amount
        $amount = new Amount();
        $amount->setCurrency('USD');
        $amount->setTotal($request->amount);
        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Your awesome 
		Product!');
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($request->return_url);
        $redirectUrls->setCancelUrl($request->return_url);
        $payment = new Payment();

        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array(
            $transaction
        ));

        $response = $payment->create($this->api_context);

        return $response;

    }

    public function executePaypal(Request $request)
    {

        $paymentId = $request->get('payment_id');
        $payerId = $request->get('payer_id');
        $payment = Payment::get($paymentId, $this->api_context);
        $paymentExecution = new PaymentExecution();
        $paymentExecution->setPayerId($payerId);

        $executePayment = $payment->execute($paymentExecution, $this->api_context);
        if ($executePayment->getState() == 'approved')
        {
            if ($executePayment->transactions[0]
                ->related_resources[0]
                ->sale->state == 'completed')
            {

				
                return response()
                    ->json(['status' => "success",

                ], 200);
            }
        }
        return response()
            ->json('failed', 400);

    }
    public function payment(Request $request, $joinid, $userid, $groupid)
    {
		$join_id = base64_decode($request->joinid);
		$user_id = base64_decode($request->userid);
		$group_id = base64_decode($request->groupid);
        $update_set = [
            "approval_status"=> "active"
        ];
        $where = [
            "id"=> $join_id,
            "user_id"=> $user_id,
            "group_id"=> $group_id,
        ];
        $get_req_details =  Joinrequests::where($where)->get()->toArray();
        $user_data= User::find($user_id);
        $group_data= Group::find($group_id);
        if($get_req_details[0]["approval_validity"] >= date("Y-m-d") && $get_req_details[0]["approval_status"] == "wait"){
			return view('frontend.paypal');
            // Joinrequests::where($where)->update($update_set);
            // $member_data = [
            //     "user_id"=> $user_id,
            //     "group_id"=> $group_id,
            //     "role_id"=> "2"
            // ];
            // Members::create($member_data);
            // $message = "Joined";
        }else if($get_req_details[0]["approval_validity"] <= date("Y-m-d")){
           	echo $message = "Link Expired";
            Joinrequests::where($where)->update(["approval_status"=> "expired"]);
        }else{
			echo "Invalid Link";
		}
        //return $message;
        
    }

    public function thankyou(Request $request)
    {
        return view('frontend.thankyou');
    }
	public function success_join(Request $request)
    {
		$join_id = base64_decode($request->joinid);
		$user_id = base64_decode($request->userid);
		$group_id = base64_decode($request->groupid);
		
		$data = [
			"user_id" => $user_id,
			"request_id" => $join_id,
			"transaction_id" => $request->transaction_id,
			"payer_email" =>  $request->payer_email_id,
			"payer_id" =>  $request->payer_id,
			"payment_for" =>  "join_request",
		];
		//dd($data);
		$update_set = [
            "approval_status"=> "active"
        ];
		$where = [
            "id"=> $join_id,
            "user_id"=> $user_id,
            "group_id"=> $group_id,
        ];
		$transaction = [];
		$transaction_arr = Transactions::where(["request_id" => $join_id,"payment_for" =>  "join_request",])->get()->toArray();
		if(isset($transaction_arr[0]) && !empty($transaction_arr)){
			$message = "Invalid Payment Link";
			$status = 400;
		}else{
			try{
				Joinrequests::where($where)->update($update_set);
				$transaction =  Transactions::create($data);
				$member_data = [
					"user_id"=> $user_id,
                    "role_id"=> "2",
					"group_id"=> $group_id
				];

				$member_arr = Members::where($member_data)->get()->toArray();
				if(isset($member_arr[0]) && !empty($member_arr[0])){
					$message = "Member all ready added ";
					$status = 400;
				}else{
					$members = Members::create($member_data);
					$message = "Fetched Successfully";
					$status = 200;
				}
			}
			catch(\Illuminate\Database\QueryException $ex){
				$message = $ex->getMessage();
				$status = 400;
			}
		}
		return response()->json([
			"data"=> $transaction,
			"status"=> $status,
			"message"=> $message
		]);
    }

}

