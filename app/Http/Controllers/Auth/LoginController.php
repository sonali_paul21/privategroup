<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo; 

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     
        if(Auth::check() && Auth::user()->id == 1){
            $this->redirectTo = route('admin.dashboard');
        } elseif(Auth::check() && Auth::user()->id != 1){
            session()->flash('message', 'Invalid credentials');
            return redirect("administrator/login");
        }
        $this->middleware('guest')->except('logout');
    }
    public function authenticated()
    {
        if(Auth::check() && Auth::user()->id == 1)
        {
            return redirect('/admin/dashboard');
        }else if(Auth::check() && Auth::user()->id != 1){
            session()->flash('message', 'Invalid credentials');
            return redirect("administrator/login");
        }
    }
    public function logout(){
        if(\Auth::check()){
            \Auth::logout();
            return redirect()->route('administrator.login');
        } else{
            return redirect()->route('frontend.login');
        }
    }
    public function login_page(){
        return view('auth.admin-login');
    }
}
