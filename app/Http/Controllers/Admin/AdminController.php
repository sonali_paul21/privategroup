<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Mail;
use App\User;
use App\Role;
use App\Permission;
use App\Group;
use App\Uploads;
use App\User_permission;
use App\Joinrequests;
use App\Cms;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Helpers\Helper;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function user_list()
    {
        //dd(Auth::id());
        return view('admin.userlist');
    }
    public function edit_user()
    {
        return view('admin.edit_user');
    }
    public function add_user()
    {
        return view('admin.add_user');
    }
    public function user_roles()
    {
        return view('admin.user_roles');
    }
    public function add_permission()
    {
        return view('admin.add_permission');
    }
    public function groups()
    {
        return view('admin.groups');
    }
    public function group_form()
    {
        return view('admin.create_group');
    }
    public function groups_details()
    {
        return view('admin.groups_details');
    }
    public function join_requests_page()
    {
        return view('admin.join_requests');
    }

    public function cms_privacy_policy()
    {
        return view('admin.cms_pages');
    }
    public function cms_about_us()
    {
        return view('admin.cms_pages');
    }
    public function cms_pages()
    {
        return view('admin.cms_pages');
    }
   
    public function cms_page_add()
    {
        return view('admin.cms_page_add');
    }

    public function render_page()
    {
        return view('admin.render_pages');
    }
    public function profile()
    {
        $user = User::with('user_permission','profile_picture')->find(1);
        return response()->json([
            "data"=> $user,
            "status"=> 200,
            "message"=> ""
        ]);
        
    }
    public function users()
    {
        $users = User::with('user_permission','profile_picture')->where('users.id','!=',1)->where("deleted","!=","1")->get()->toArray();
        return response()->json([
            "data"=> $users,
            "status"=> 200,
            "message"=> ""
        ]);
    }
   
    public function create_user(Request $data)
    {
        $user = [];
        $validator = Validator::make($data->all(), [
            'user_name' => 'required',
            'email_address' => 'required |email|unique:users,email',
            'contact_number' => 'required|min:10|max:10|unique:users,contact_number',
            'dob'=> 'required',
            'gender'=> 'required',
            'password' => 'required|min:6',
            'user_role'=> 'required'
        ]);

		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $user = [
            'name' => $data->user_name,
            'email' => $data->email_address,
            'contact_number'=>$data->contact_number,
            'dob'=>$data->dob,
            'gender'=>$data->gender,
            'password' => Hash::make($data->password),
            'deleted'=> '0'
        ];
        try{
            $user = User::create($user);
            $message = "User Created Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }

        if($status == 200 ){
            foreach($data->user_permission as $value){
                User_permission::create(["user_id"=> $user->id,"permission_id"=>$value,"role_id"=>$data->user_role ]);
            }
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $user 
        ]);
    }

    public function get_user(Request $request){
        $id = $request->route('id');
        $get_user = [];
        try{
            $get_user = User::with("profile_picture","user_permission","user_details","user_education_details")->find($id);
            $message = "Get user Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $get_user 
        ]);
    }

    public function update_user(Request $request){
        $validator = Validator::make($request->all(), [
            "id"=> "required",
            "user_name" => "required",
            "email_address"=> "required",
            "contact_number"=> "required",
            "dob"=> "required"
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $update_set = [
            "name"=> $request->user_name,
            "email"=> $request->email_address,
            "contact_number"=> $request->contact_number,
            "dob"=> $request->dob
        ];
        $update = [];
        $data = [];
        $data_arr = [];
        try {
            $update = User::where("id",$request->id)->update($update_set);
            $data_arr = $this->get_user($request);
            if($data_arr->original["status"] == 200){
                $data = $data_arr->original["data"];
            }
            
            $status = 200;
            $message ="Updated Successfully";
        }  catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $data
        ]);
        
    }

    public function user_password(Request $request){
        $id = $request->route('id');
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|max:8'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $update = 0;
        try{
            $update = User::where("id",$id)->update(["password"=>Hash::make($request->password)]);
            $message = "Password Changed Successfully";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $update
        ]);
        
    }
    public function delete_user(Request $request){
        $user = [];
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        try{
            $delete_user= User::where("id",$request->id)->update(["deleted"=>"1"]);
            $message = "Deleted Successfully";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        $data_set = [];
        
        if($status == 200){
            $data = $this->users();
            if($data->original['status'] == 200){
                $data_set = $data->original['data']; 
            }
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $data_set
        ]);
    }
    public function roles()
    {
        $roles = Role::where("id","!=",1)->get();
        return response()->json([
            "data"=> $roles,
            "status"=> 200,
            "message"=> ""
        ]);
    }
    public function create_role(Request $data)
    {
        $role = ucfirst($data->role_name);
        $data->replace(['role_name' => $role]);
        $validator = Validator::make($data->all(), [
            'role_name' => 'required|unique:roles,role_name'
        ]);

		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $role_slug = str_replace(" ","_",strtolower($data->role_name));
        $role = [
            'role_name' => ucfirst($data->role_name),
            'role_slug' =>  $role_slug
        ];
        try{
            Role::create($role);
            $message = "Role Created Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;

        }
        return response()->json([
            "message" => $message,
            "status"=> $status
        ]);
    }
    public function permissions()
    {
        $permissions = Permission::get();
        return response()->json([
            "data"=> $permissions,
            "status"=> 200,
            "message"=> ""
        ]);
    }
    public function create_permission(Request $data)
    {
        $permission = ucfirst($data->permission_name);
        $data->replace(['permission_name' => $permission]);
        $validator = Validator::make($data->all(), [
            'permission_name' => 'required|unique:permission,permission_name'
        ]);

		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $permission_slug = str_replace(" ","_",strtolower($data->permission_name));
        $permission = [
            'permission_name' => ucfirst($data->permission_name),
            'permission_slug' =>  $permission_slug
        ];
        try{
            Permission::create($permission);
            $message = "Permission Created Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;

        }
        return response()->json([
            "message" => $message,
            "status"=> $status
        ]);
    }


    public function create_group(Request $data)
    {
        $validator = Validator::make($data->all(), [
            'group_name' => 'required|unique:groups,group_name',
            'group_charge'=> 'required'
        ]);

		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $group_slug_lower = strtolower($data->group_name);
        $group_slug = str_replace(" ", "-", trim($group_slug_lower));
        $group = [
            'group_name' => $data->group_name,
            'group_slug' => $group_slug,
            'group_charge' =>  $data->group_charge
        ];
        try{
            $data = Group::create($group);
            $message = "Group Created Successfully!";
            $status = 200;

            
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
            $data = [];
        }
        
        // if($status == 200){
        //     $data_set = [
        //         'module_name' => 'group',
        //         'respective_id' =>  $data->id,
        //         'type'=>'profile_image',
        //         'path'=> 'images/noimage.jpg'
        //     ];
        //     try{
        //         Uploads::create($data_set);
        //         $message = "User Created Successfully!";
        //         $status = 200;
        //     }catch(\Illuminate\Database\QueryException $ex){
        //         $message = $ex->getMessage();
        //         $status = 400;
        //     }
        // }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data" => $data
        ]);
    }

    public function uploads(Request $data){

        $uploads = [];
        $validator = Validator::make($data->all(), [
            'file' => 'required',
            'module_name'=> 'required',
            'respective_id' => 'required',
            'type'=> 'required'
        ]);

        $file = $data->file('file');
   
        $destinationPath = 'uploads/'.$data->module_name.'/'.$data->type;
        File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
        $file_name = time().rand(1,100).$file->getClientOriginalName();
        $file->move('public/'.$destinationPath,$file_name);

        $data_set = [
            'module_name' => $data->module_name,
            'respective_id' =>  $data->respective_id,
            'type'=>$data->type
        ];
        $uploads = Uploads::where($data_set)->get()->toArray();
        $data_set['path'] = $destinationPath.'/'.$file_name;
        $data_arr = [];
        try{
            if(!empty($uploads)){
                $data_arr = Uploads::where('id',$uploads[0]['id'])->update(['path' => $data_set['path']]);
                $data_arr = Uploads::find($uploads[0]['id']);
            }else{
                $data_arr = Uploads::create($data_set);
            }
            $message = "Uploaded Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
            $data = [];
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data" => $data_arr
        ]);

    }
    public function all_groups()
    {
        $groups = Group::with('profile_picture')->where("is_active","0")->get()->toArray();
        return response()->json([
            "data"=> $groups,
            "status"=> 200,
            "message"=> ""
        ]);
    }
    public function get_group(Request $request){
        $id = $request->route('id');
        $get_user = [];
        try{
            $get_user = Group::with('profile_picture','members')->find($id);
            $message = "Get group Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $get_user 
        ]);
    }

    public function delete_group(Request $request){
        $group = [];
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        try{
            $delete_group= Group::where("id",$request->id)->update(["is_active"=>"1"]);
            $message = "Deleted Successfully";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        $data_set = [];
        
        if($status == 200){
            $data = $this->all_groups();
            if($data->original['status'] == 200){
                $data_set = $data->original['data']; 
            }
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $data_set
        ]);
        
    }
    public function update_group(Request $request){
        $id = $request->route('id');
        $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'group_charge'=> 'required'
        ]);

		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $group = [
            'group_name' => $request->group_name,
            'group_charge' =>  $request->group_charge
        ];
        $grp_data = [];
        try{
            $data = Group::where("id",$id)->update($group);
            $message = "Updated Successfully!";
            $status = 200;
            //$grp_data = $this->get_group($request);
            
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
            $data = "";
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $data 
        ]);
    }

    public function join_requests(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        $join_requests = [];
        try{
            $join_requests =  Joinrequests::with("user")->where("group_id",$request->id)->where("approval_status","!=","decline")->where("approval_status","!=","active")->get()->toArray();
            $message = "Fetched Successfully";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $join_requests
        ]);
    }
    public function approve_request(Request $request){
        $message = "";
        $status = 400;
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'group_id'=>'required',
            'user_id'=> 'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }

        $user = User::find($request->user_id);
        $msg = env('APP_URL')."/".base64_encode($request->id).'/'.base64_encode($request->user_id)."/groupjoin/payment/".base64_encode($request->group_id);

        $mail_data = [
            "to" => $user->email,
            "name" => $user->name,
            "subject"=> "Private Group - Payment Link ",
            "link"=> $msg
        ];
       
       // mail($user->email,"Pay your group joining Charge",$msg);
        Mail::send('email_templates.payment-link', ['mail_data' => $mail_data], function ($m) use ($mail_data) {
            $m->from('hello@app.com', $mail_data['subject']);
            $m->to($mail_data['to'], $mail_data['name'])->subject($mail_data['subject']);
        });
        $data = [
            'approved_by'=> "1",
            'approval_validity'=> date("Y-m-d",strtotime("+7 days")),
            'approval_status'=> "wait"
        ];
        try{
            $join_requests =  Joinrequests::where("id",$request->id)->update($data);
            $message = "Fetched Successfully";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        
        return response()->json([
            "message" => "Approved ! Payment link sent to user email id .",
            "status"=> $status,
            "data"=> $msg
        ]);
    }

    public function decline_join_request(Request $request)
    {
        $message = "";
        $status = 400;
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'group_id'=>'required',
            'user_id'=> 'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        
       
        try{
            $join_request = Joinrequests::where($request->all())->update(["approval_status"=>"decline"]);
            $message = "Join Request Has been declined !";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> []
        ]);
    }

    public function all_join_requests()
    {
        $join_requests = [];

        try{
            $join_requests =  Joinrequests::with(["user","group"])->where("approval_validity",">",date("Y-m-d"))->where("approval_status","pending")->get()->toArray();
            $message = "Get all join request";
            $status = 200;
        }
        catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $join_requests
        ]);

    }

    public function store_cms(Request $request){
        $message = "";
        $status = 400;
        $validator = Validator::make($request->all(), [
            'content_type'=>'required',
            'content'=> 'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        try{
            $data = Cms::where(["content_type" => $request->content_type])->get()->toArray();
            if(!empty($data)){
                Cms::where(["content_type" => $request->content_type])->update(["content"=>$request->content]);
            }else{
                Cms::create([
                    "content_type"=> $request->content_type,
                    "content"=> $request->content
                ]);
            }
            $message = "Stored Successfully !";
            $status = 200;
            
        } catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }

        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> Cms::where(["content_type" => $request->content_type])->get()->toArray()
        ]);
        
    }
    public function get_cms(Request $request){
        $message = "";
        $status = 400;
        $validator = Validator::make($request->all(), [
            'content_type'=>'required'
        ]);
		if($validator->fails()){
            return response()->json([
                "message" => $validator->errors()->first(),
                "status" => 400,
            ]);
        }
        try{
            $data = Cms::where(["content_type" => $request->content_type])->get()->toArray();
            $message = "Stored Successfully !";
            $status = 200;
            
        } catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
        }

        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data"=> $data
        ]);
        
    }

    public function upload_cms(Request $data){

        $uploads = [];
        $validator = Validator::make($data->all(), [
            'file' => 'required',
            'module_name'=> 'required'
        ]);

        $file = $data->file('image');
   
        $destinationPath = 'uploads/'.$data->module_name;
        File::isDirectory($destinationPath) or File::makeDirectory($destinationPath, 0777, true, true);
        $file_name = time().rand(1,100).$file->getClientOriginalName();
        $file->move('public/'.$destinationPath,$file_name);

        $data_set = [
            'module_name' => $data->module_name,
            'respective_id'=> 0,
            "type"=>"content_image"
        ];
        $uploads = Uploads::where($data_set)->get()->toArray();
        $data_set['path'] = $destinationPath.'/'.$file_name;
        $data_arr = [];
        try{
            if(!empty($uploads)){
                $data_arr = Uploads::where('id',$uploads[0]['id'])->update(['path' => $data_set['path']]);
                $data_arr = Uploads::find($uploads[0]['id']);
            }else{
                $data_arr = Uploads::create($data_set);
            }
            $message = "Uploaded Successfully!";
            $status = 200;
        }catch(\Illuminate\Database\QueryException $ex){
            $message = $ex->getMessage();
            $status = 400;
            $data = [];
        }
        return response()->json([
            "message" => $message,
            "status"=> $status,
            "data" => $data_arr
        ]);

    }

}
