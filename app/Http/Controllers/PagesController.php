<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function login()
    {
        return view('frontend.login');
    }
    public function signup(){
        return view('frontend.signup');
    }
    public function home(){
        return view('frontend.home');
    }
    public function profile(){
        return view('frontend.profile');
    }
    // public function group(){
    //     return view('frontend.group');
    // }
    // public function group_member(){
    //     return view('frontend.group_member');
    // }
    // public function privacy_policy(){
    //     return view('frontend.privacy_policy');
    // }
    // public function about_us(){
    //     return view('frontend.about_us');
    // }

    public function render_frontend(){
        return view('frontend.profile');
    }
}
