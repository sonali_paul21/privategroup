<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_permission extends Model
{
    protected $table = 'user_permission';

    protected $fillable = [
        'user_id', 'permission_id','role_id'
    ];
    public function permissions()
    {
        return $this->belongsTo('App\Permission','permission_id');
    }
    public function roles()
    {
        return $this->belongsTo('App\Role','role_id');
    }
}
