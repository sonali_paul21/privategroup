<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    protected $fillable = [
        'user_id', 'group_id','booking_member_id','date_time_of_appointment','booking_amount'
    ];
}
